---
- name: Initial DigitalOcean Ubuntu server provisioning
  hosts: ["webserver"]

  vars_files:
    - "environments/default.yml"

  tasks:
    - name: Update and upgrade apt packages
      apt:
        upgrade: yes
        update_cache: yes
        
    - name: Create group {{ default_user }} for user {{ default_user }}
      group:
        name: "{{ default_user }}"
        state: present

    - name: "Create default user {{ default_user }} and add it to 'sudo' group"
      user:
        name: "{{ default_user }}"
        shell: /bin/bash
        group: "{{ default_user }}"
        groups: sudo
        append: yes

    - name: "Delete password for user {{ default_user }}"
      command: "passwd --delete {{ default_user }}"

    - name: "Disable password-based access for user root"
      user:
        name: root
        password_lock: yes

    - name: "Disable password for sudo's group"
      copy:
        src: "91-custom"
        dest: "/etc/sudoers.d/"
        mode: 0400

    - name: "Create directory for ssh keys in {{ default_user }} home directory"
      file:
        path: "/home/{{ default_user }}/.ssh"
        state: directory
        mode: 0700
        owner: "{{ default_user }}"
        group: "{{ default_user }}"

    - name: "Copy authorized keys from root ssh directory to {{ default_user }} ssh directory"
      copy:
        src: /root/.ssh/authorized_keys
        dest: "/home/{{ default_user }}/.ssh"
        remote_src: yes
        mode: preserve
        owner: "{{ default_user }}"
        group: "{{ default_user }}"

    - name: "Remove authorized keys from root ssh directory"
      file:
        path: /root/.ssh/authorized_keys
        state: absent

    - name: "Disable root SSH login with password"
      replace:
        path: /etc/ssh/sshd_config
        regexp: '^PermitRootLogin.*'
        replace: 'PermitRootLogin prohibit-password'
        backup: yes
    
    - name: "Restart SSH demon"
      command: "systemctl restart sshd"
    