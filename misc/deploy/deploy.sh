#!/bin/bash

VHOST_SITE_ROOT=$1

set -e

rm -rf /tmp/dist
unzip /tmp/dist.zip -d /tmp/ 
rm /tmp/dist.zip
rm -rf "$VHOST_SITE_ROOT.bck"
if [ -d $VHOST_SITE_ROOT ]; then
  mv $VHOST_SITE_ROOT "$VHOST_SITE_ROOT.bck"
fi
mv /tmp/dist $VHOST_SITE_ROOT
