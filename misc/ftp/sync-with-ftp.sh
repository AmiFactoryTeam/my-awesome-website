#!/bin/bash

# This is a simple bash script, which call lftp utility to sync 
# your local website directory (LCD) with remote host website directory(RCD)

# FTP username (replace with your ftp-user login)
USER=ftp-user
# FTP user password (replace with your ftp-user password)
PASS=my-secret-password
# Website address (replace with your website domain)
HOST="example.com"
# Website local source directory (dist) (replace with your project path)
LCD="/<path to your project on your laptop>/dist"
# FTP target directory (related to FTP root) (usually the same as domain name, exampe.com)
RCD="example.com"

lftp -f "
open $HOST
user $USER $PASS
lcd $LCD
mirror --continue --reverse --delete --verbose $LCD $RCD
bye
" 
