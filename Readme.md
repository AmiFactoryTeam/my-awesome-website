# This is a demo website project AmiFactory Team Blog post 'How to deploy a website'

[![pipeline status](https://gitlab.com/AmiFactoryTeam/my-awesome-website/badges/master/pipeline.svg)](https://gitlab.com/AmiFactoryTeam/my-awesome-website/commits/master)

You can find more information in this [blog post](https://amifactory.team/blog/how-to-deploy-a-website/)

[Live demo](https://myawesomewebsite.afstaging.network/)

## Preparations
1. Set your remote host IP address:
* file **infra/inventory/webserver**
```
webserver ansible_host=<remote host IP address>
```
* file **infra/environments/webserver.yml**
```
host_ip: "<remote host IP address>"
```

2. Set your domain name in file file **infra/environments/webserver.yml**:
```
domain_record: "<website domain name>"
```

3. If you plan to use FTP, enable it install in **infra/environments/webserver.yml**:
```
ftp_use: true
```


## (Optional) Run Ansible in docker container locally if you don't have Ansible installed locally, but have Docker:
```
docker run --rm -v <private key file>:/root/.ssh/id_rsa -v <project dir>:/infra --env-file <docker env file> -it  registry.gitlab.com/amifactoryteam/docker-ansible:latest
```

were:
* **'private key file'** - absolute path to a ssh private key. This key will be used to get ssh-access for Ansible to connect to target host;
* **'project dir'** - absolute path to directory, that contains Ansible playbook(s);
* **'docker env file'** - absolute path to .docker-env file. This file usually contains environment and secret variables, that Ansible will use during target host provisioning. See example below.

After you run Ansible Image, you need to prepare the ssh connection. Run following:
* Run ssh deamon:
```
eval $(ssh-agent -s)
```
* Add your private key to ssh deamon:
```
ssh-add
```
* Move to 'infra' directory:
```
cd /infra
```


## Run initial provisioning of DigitalOcean Ubuntu 18.04 droplet
```
ansible-playbook -i inventory/webserver -e 'ansible_ssh_user=root' -v playbook-initial.yml
```


## Run web server provisioning
**Please notice, that before running web server provisioning ensure, that your domain points to your host IP address (DNS A record exists).**
```
ansible-playbook -i inventory/webserver -v playbook.yml
```


## **.docker_env** file example
```
# Let's Encrypt
MAW_LE_ADMIN_EMAIL=<Email address for LetsEncrypt>

# FTP
MAW_FTP_USER='<User name for FTP>'
MAW_FTP_USER_PASS='<User password for FTP>'
```

## Required GitLab secret variables
```
MAW_HOST='<IP address of remote host>'
MAW_HOST_USER='<User name for login to remote host>'
MAW_HOST_SSH_PRIVATE_KEY='<Content of a private key, used for SSH access to remote host>'
```
